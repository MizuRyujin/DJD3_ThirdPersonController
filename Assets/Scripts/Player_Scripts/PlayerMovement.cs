﻿using UnityEngine;

namespace Scripts
{
    public class PlayerMovement : MonoBehaviour
    {
        #region Constants
        // Acceleration values
        private const float _MAX_ANGULAR_ACCELERATION = 90.0f;
        private const float _GRAVITY_ACCELERATION = -20.0f;
        private const float _MAX_FORWARD_ACCELERATION = 10.0F;
        private const float _MAX_BACKWARDS_ACCELERATION = 10.0F;
        private const float _JUMP_ACCELERATION = 500.0F;
        private const float _MAX_STRAFE_ACCELERATION = 10.0F;

        // Velocity values
        private const float _MOUSE_ANGULAR_VELOCITY_FACTOR = 5.0f;
        private const float _MAX_ANGULAR_VELOCITY = 60.0f;
        private const float _MAX_FORWARD_VELOCITY = 4.0F;
        private const float _MAX_BACKWARDS_VELOCITY = -2.0F;
        private const float _MAX_STRAFE_VELOCITY = 3.0F;
        private const float _MAX_JUMP_VELOCITY = 50.0F;
        private const float _MAX_FALL_VELOCITY = -30.0F;

        // Factors
        private const float _WALK_VELOCITY_FACTOR = 1.0F;
        private const float _RUN_VELOCITY_FACTOR = 2.0F;
        #endregion

        // Class variables
        private CharacterController _pController = default;
        private Vector3 _acceleration = default;
        private Vector3 _velocity = default;
        private Vector3 _motion = default;
        private float _angularAcceleration = default;
        private float _angularVelocity = default;
        private float _angularMotion = default;
        private float _velocityFactor = default;
        private bool _jump = default;
        private bool _autoRun = default;
        private bool _mouseWalk = default;

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
            _pController = GetComponent<CharacterController>();
            _acceleration = Vector3.zero;
            _velocity = Vector3.zero;
            _motion = Vector3.zero;
            _angularAcceleration = 0.0f;
            _angularVelocity = 0.0f;
            _velocityFactor = _RUN_VELOCITY_FACTOR;
            _jump = false;
            _autoRun = false;
            _mouseWalk = false;
        }
        #region Update
        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        private void Update()
        {
            CheckForJump();
            UpdateVelocityFactor();
            UpdateAutoRun();
            UpdateMouseWalk();
            UpdateRotation();
            UpdateMouseLock();
        }

        private void CheckForJump()
        {
            if (_pController.isGrounded && Input.GetButtonDown("Jump"))
            {
                _jump = true;
            }
        }

        private void UpdateVelocityFactor()
        {
            if (Input.GetButtonDown("Walk"))
            {
                if (_velocityFactor == _RUN_VELOCITY_FACTOR)
                {
                    _velocityFactor = _WALK_VELOCITY_FACTOR;
                }
                else
                {
                    _velocityFactor = _RUN_VELOCITY_FACTOR;
                }
            }

            // _velocityFactor = _autoRun || !Input.GetButton(
            //     "Walk") ? _RUN_VELOCITY_FACTOR : _WALK_VELOCITY_FACTOR;
        }

        private void UpdateAutoRun()
        {
            if (Input.GetButtonDown("AutoFlyYouFools"))
            {
                _autoRun = !_autoRun;
            }
            else if (_autoRun && (Input.GetAxis("Forward") != 0 || _mouseWalk))
            {
                _autoRun = false;
            }
        }

        private void UpdateMouseWalk()
        {
            _mouseWalk = (Input.GetMouseButton(0) && Input.GetMouseButton(1))
                || Input.GetMouseButton(2);
        }

        private void UpdateRotation()
        {
            if (Input.GetMouseButton(1))
            {
                UpdateMouseRotation();
            }
            else
            {
                UpdateKeyRotation();
            }
        }

        private void UpdateKeyRotation()
        {
            _angularAcceleration = Input.GetAxis(
                    "Rotate") * _MAX_ANGULAR_ACCELERATION * _velocityFactor;

            if (_angularAcceleration != 0)
            {
                _angularVelocity += _angularAcceleration * Time.deltaTime;

                _angularVelocity = Mathf.Clamp(
                    _angularVelocity,
                    -_MAX_ANGULAR_VELOCITY * _velocityFactor,
                    _MAX_ANGULAR_VELOCITY * _velocityFactor);

                _angularMotion = _angularVelocity * Time.deltaTime;

                transform.Rotate(0.0f, _angularMotion, 0.0f);
            }

            else
            {
                _angularVelocity = 0.0f;
            }
        }

        private void UpdateMouseRotation()
        {
            _angularMotion = Input.GetAxis(
                "Mouse X") * _MOUSE_ANGULAR_VELOCITY_FACTOR;

            transform.Rotate(0.0f, _angularMotion, 0.0f);
        }

        private void UpdateMouseLock()
        {
            if (Input.GetMouseButtonDown(1))
            {
                Cursor.lockState = CursorLockMode.Locked;
            }

            else if (Input.GetMouseButtonUp(1))
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }

        #endregion

        #region Fixed Update
        /// <summary>
        /// This function is called every fixed framerate frame, 
        /// if the MonoBehaviour is enabled.
        /// </summary>
        private void FixedUpdate()
        {
            UpdateAcceleration();
            UpdateVelocity();
            UpdatePosition();
        }

        /// <summary>
        /// Method to update player acceleration
        /// </summary>
        private void UpdateAcceleration()
        {
            _acceleration.z =
                _autoRun || _mouseWalk ? 1.0f : Input.GetAxis("Forward");

            _acceleration.z *= _velocityFactor * (_acceleration.z > 0 ?
                 _MAX_FORWARD_ACCELERATION : _MAX_BACKWARDS_ACCELERATION);

            _acceleration.x = Input.GetAxis("Strafe");

            _acceleration.x *= (_velocityFactor * _MAX_STRAFE_ACCELERATION);

            if (_jump)
            {
                _jump = false;
                _acceleration.y = _JUMP_ACCELERATION;
            }
            else if (_pController.isGrounded)
            {
                _acceleration.y = 0.0f;
            }
            else
            {
                _acceleration.y = _GRAVITY_ACCELERATION;
            }
        }

        /// <summary>
        /// Method to update player velocity (considers acceleration vector)
        /// </summary>
        private void UpdateVelocity()
        {
            _velocity += _acceleration * Time.fixedDeltaTime;

            _velocity.z = _acceleration.z == 0.0f ? 0.0f : Mathf.Clamp(
                _velocity.z, _MAX_BACKWARDS_VELOCITY * _velocityFactor,
                _MAX_FORWARD_VELOCITY * _velocityFactor);

            _velocity.x = _acceleration.x == 0.0f ? 0.0f : Mathf.Clamp(
                _velocity.x, -_MAX_STRAFE_VELOCITY * _velocityFactor,
                _MAX_STRAFE_VELOCITY * _velocityFactor);

            _velocity.y = _acceleration.y == 0.0f ? -0.1f : Mathf.Clamp(
                _velocity.y, _MAX_FALL_VELOCITY, _MAX_JUMP_VELOCITY);
        }

        /// <summary>
        /// Method to update player position (considers velocity vector)
        /// </summary>
        private void UpdatePosition()
        {
            if (_pController.enabled)
            {
                _motion = transform.TransformVector(
                    _velocity * Time.fixedDeltaTime);

                _pController.Move(_motion);
            }
        }
        #endregion

        public PlayerSaveData CreatePlayerSaveData()
        {
            PlayerSaveData saveData = new PlayerSaveData();

            saveData.position = transform.position;
            saveData.rotation = transform.rotation;

            return saveData;
        }

        public void ProcessSaveData(PlayerSaveData saveData)
        {
            _pController.enabled = false;

            transform.position = saveData.position;
            transform.rotation = saveData.rotation;

            _pController.enabled = true;
        }
    }

    [System.Serializable]
    public struct PlayerSaveData
    {
        public Vector3 position;
        public Quaternion rotation;
    }
}

