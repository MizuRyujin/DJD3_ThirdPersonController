using UnityEngine;

namespace Scripts
{
    public class CameraMovement : MonoBehaviour
    {
        #region  Class constants
        // Rotation values
        private const float _MOUSE_ANGULAR_VELOCITY_FACTOR = 5.0f;
        private const float _MIN_ROTATION_X = 335.0f;
        private const float _MAX_ROTATION_X = 85.0f;

        // Zoom values
        private const float _TRANSLATION_ACCELERATION = 500.0f;
        private const float _TRANSLATION_DECELERATION = 10.0f;
        private const float _MAX_TRANSLATION_VELOCITY = 10.0f;
        private const float _MAX_ZOOM_DISTANCE = -15.0f;

        // Camera obstacle move values
        private const float _AUTO_ADJUST_VELOCITY = 10.0f;
        #endregion

        private float _minZoomDistance = default;

        // Class variables
        /// <summary>
        /// Camera object reference
        /// </summary>
        private Transform _sensor = default;

        /// <summary>
        /// Player's character controller reference
        /// </summary>
        private CharacterController _pController = default;

        /// <summary>
        /// Camera rotation value, with mouse movement
        /// </summary>
        private Vector3 _newRotation = default;

        /// <summary>
        /// Camera zoom position, with mouse scroll wheel
        /// </summary>
        private Vector3 _newPosition = default;

        /// <summary>
        /// Variable to store original camera position, before an obstacle came
        /// in between
        /// </summary>
        private Vector3 _intendedPosition = default;

        /// <summary>
        /// Information about what has the ray cast hit
        /// </summary>
        private RaycastHit _raycastHitInfo = default;

        /// <summary>
        /// Zoom acceleration value
        /// </summary>
        private float _translation_Accel = default;

        /// <summary>
        /// Zoom velocity value
        /// </summary>
        private float _translation_Velocity = default;

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
            _sensor = transform.GetChild(0);
            _minZoomDistance = _sensor.GetChild(0).transform.localPosition.z;
            _pController = GetComponentInParent<CharacterController>();
            _intendedPosition = _sensor.transform.localPosition;
            _translation_Accel = 0.0f;
            _translation_Velocity = 0.0f;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        private void Update()
        {
            UpdateCameraRotation();
            UpdateCameraPosition();
            PreventCharacterOcclusion();
            UpdateMouseLock();

            ResetCameraPosition();
        }

        /// <summary>
        /// Method to control camera rotation with mouse buttons
        /// </summary>
        private void UpdateCameraRotation()
        {
            _newRotation = transform.localEulerAngles;

            if (Input.GetMouseButton(0) && !Input.GetMouseButton(1))
            {
                _newRotation.y +=
                    Input.GetAxis("Mouse X") * _MOUSE_ANGULAR_VELOCITY_FACTOR;
            }

            if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
            {
                _newRotation.x -=
                    Input.GetAxis("Mouse Y") * _MOUSE_ANGULAR_VELOCITY_FACTOR;

                _newRotation.x =
                    _newRotation.x > 180f ? Mathf.Max(
                        _newRotation.x, _MIN_ROTATION_X) :
                         Mathf.Min(_newRotation.x, _MAX_ROTATION_X);
            }
            transform.localEulerAngles = _newRotation;
        }

        /// <summary>
        /// Method to reset camera rig rotation when left mouse button 
        /// is released
        /// </summary>
        private void ResetCameraPosition()
        {
            //! This should have way to check if the game is being reloaded
            if (!Input.GetMouseButton(0) && !Input.GetMouseButton(1))
            {
                transform.localRotation = Quaternion.Lerp(
                                    transform.localRotation,
                                    Quaternion.identity, 2f * Time.deltaTime);
            }
        }

        /// <summary>
        /// Method to control camera zoom
        /// </summary>
        private void UpdateCameraPosition()
        {
            _newPosition = _sensor.localPosition;

            _translation_Accel =
                Input.GetAxis("Zoom") * _TRANSLATION_ACCELERATION;

            if (_translation_Accel != 0)
            {
                _translation_Velocity = Mathf.Clamp(
                    _translation_Velocity + _translation_Accel * Time.deltaTime,
                    -_MAX_TRANSLATION_VELOCITY,
                    _MAX_TRANSLATION_VELOCITY);

            }
            else if (_translation_Velocity > 0.0f)
            {
                _translation_Velocity = Mathf.Max(
                    _translation_Velocity -
                    _TRANSLATION_DECELERATION * Time.deltaTime, 0.0f);
            }
            else if (_translation_Velocity < 0.0f)
            {
                _translation_Velocity = Mathf.Min(
                    _translation_Velocity +
                    _TRANSLATION_DECELERATION * Time.deltaTime, 0.0f);
            }

            if (_translation_Velocity != 0.0f)
            {
                _newPosition.z = Mathf.Clamp(
                    _newPosition.z + _translation_Velocity * Time.deltaTime,
                    _MAX_ZOOM_DISTANCE, -_minZoomDistance);

                _sensor.localPosition = _intendedPosition = _newPosition;
            }
        }

        /// <summary>
        /// Method to ensure character isn't hidden by objects
        /// </summary>
        private void PreventCharacterOcclusion()
        {
            if (Physics.Linecast(transform.position,
                transform.TransformPoint(_intendedPosition),
                out _raycastHitInfo))
            {
                if (_sensor.localPosition.z + _minZoomDistance < 0.0f)
                {
                    _sensor.position = Vector3.Lerp(
                        _sensor.position,
                        _raycastHitInfo.point,
                        _AUTO_ADJUST_VELOCITY * Time.deltaTime);
                }
            }
            else
            {
                _sensor.localPosition = Vector3.Lerp(
                    _sensor.localPosition,
                    _intendedPosition,
                    _AUTO_ADJUST_VELOCITY * Time.deltaTime);
            }
        }

        /// <summary>
        /// Method to hide cursor when player moves camera
        /// </summary>
        private void UpdateMouseLock()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Cursor.lockState = CursorLockMode.Locked;
            }

            else if (Input.GetMouseButtonUp(0))
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }

        public CameraSaveData CreateCameraSaveData()
        {
            CameraSaveData saveData = new CameraSaveData();

            saveData.position = _sensor.localPosition;
            saveData.rotation = transform.rotation;

            return saveData;
        }

        public void ProcessSaveData(CameraSaveData saveData)
        {
            _sensor.localPosition = _newPosition = _intendedPosition = saveData.position;
            transform.rotation = saveData.rotation;
        }
    }

    [System.Serializable]
    public struct CameraSaveData
    {
        public Vector3 position;
        public Quaternion rotation;
    }
}