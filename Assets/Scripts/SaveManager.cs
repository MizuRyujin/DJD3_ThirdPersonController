﻿using System.IO;
using UnityEngine;

namespace Scripts
{
    public class SaveManager : MonoBehaviour
    {
        private const string SAVE_FILENAME = "save.dat";
        private const KeyCode SAVE_HOTKEY = KeyCode.F5;
        private const KeyCode LOAD_HOTKEY = KeyCode.F6;

        private string _saveFilePath;

        [SerializeField] private PlayerMovement playerMovement = default;
        [SerializeField] private CameraMovement cameraMovement = default;

        [System.Serializable]
        private struct SaveData
        {
            public PlayerSaveData playerSaveData;
            public CameraSaveData cameraSaveData;
        }

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
            _saveFilePath = Application.persistentDataPath + "/" + SAVE_FILENAME;
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetKeyDown(SAVE_HOTKEY)) SaveGame();
            else if (Input.GetKeyDown(LOAD_HOTKEY)) LoadGame();
        }

        private void SaveGame()
        {
            SaveData saveData = CreateSaveData();

            StoreSaveData(saveData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private SaveData CreateSaveData()
        {
            SaveData saveData = new SaveData();

            saveData.playerSaveData = playerMovement.CreatePlayerSaveData();
            saveData.cameraSaveData = cameraMovement.CreateCameraSaveData();

            return saveData;
        }

        /// <summary>
        /// Method to store the save data to a file
        /// </summary>
        /// <param name="saveData"> Data to be saved </param>
        private void StoreSaveData(SaveData saveData)
        {
            string jsonSaveData = JsonUtility.ToJson(saveData, true);

            File.WriteAllText(_saveFilePath, jsonSaveData);

            print("Game saved !");
        }

        private void LoadGame()
        {
            if (File.Exists(_saveFilePath))
            {
                SaveData saveData = LoadSaveData();

                ProcessSaveData(saveData);
            }
        }

        private SaveData LoadSaveData()
        {
            string jsonSaveData = File.ReadAllText(_saveFilePath);

            SaveData saveData = JsonUtility.FromJson<SaveData>(jsonSaveData);

            return saveData;
        }

        private void ProcessSaveData(SaveData saveData)
        {
            playerMovement.ProcessSaveData(saveData.playerSaveData);
            cameraMovement.ProcessSaveData(saveData.cameraSaveData);
            print("Game loaded !");
        }
    }
}
